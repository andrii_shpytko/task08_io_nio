package com.epam.controller;

import com.epam.model.ReaderUtils;
import com.epam.model.SerializeUtils;
import com.epam.model.WriterUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private SerializeUtils serializeUtils;
    private ReaderUtils readerUtils;
    private WriterUtils writerUtils;

    public Controller() {
        serializeUtils = new SerializeUtils();
        readerUtils = new ReaderUtils();
        writerUtils = new WriterUtils();
    }
}
