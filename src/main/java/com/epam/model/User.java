package com.epam.model;

import java.io.Serializable;

public class User implements Serializable {
    private String name;
    private String lastName;
    private transient String surName;
    private int age;
    private String country;
    static int staticValue;

    public User(String name, String lastName, String surName, int age, String country) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.country = country;
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSurName() {
        return surName;
    }

    public int getAge() {
        return age;
    }

    public String getCountry() {
        return country;
    }

    public static int getStaticValue() {
        return staticValue;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", surName='" + surName + '\'' +
                '}';
    }
}
