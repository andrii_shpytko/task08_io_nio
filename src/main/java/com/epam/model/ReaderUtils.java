package com.epam.model;

import org.apache.commons.lang.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReaderUtils {
    private static final String FILE_NAME = "";
    private static final Logger LOGGER = LogManager.getLogger(ReaderUtils.class);

    public List<User> readObject(String fileName) {
        List<User> users = new ArrayList<>();
        try (ObjectInputStream objInput = new ObjectInputStream(
                Files.newInputStream(Paths.get(fileName))
        )) {
            while (true) {
                User user = (User) objInput.readObject();
                if (!"".equals(user.getName())) {
                    users.add(user);
                } else {
                    break;
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }

    public void usualReading() {
        try (FileInputStream inputStream = new FileInputStream(
                new File(this.getClass().getResource("/40mb.txt").toURI())
        )) {
            while (inputStream.read() != -1) {
            }
        } catch (FileNotFoundException | URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReading() {
        try (BufferedInputStream inputStream = new BufferedInputStream(
                new FileInputStream(
                        new File(this.getClass().getResource("/200mb.txt").toURI()))
        )) {
            while (inputStream.read() != -1) {
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReadingToSize(int size) {
        try (BufferedInputStream inputStream = new BufferedInputStream(
                new FileInputStream(new File(this.getClass().getResource("/200mb.txt").toURI())), size
        )) {
            while (inputStream.read() != -1) {
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        final long startTime = System.nanoTime();
        StopWatch timer = new StopWatch();
        timer.start();
        new ReaderUtils().bufferedReadingToSize(2400);
        timer.stop();
        long time = timer.getTime();
        final long duration = System.nanoTime() - startTime;
        LOGGER.info(time);
        LOGGER.info(duration / 1_000_000_000);
    }
}
