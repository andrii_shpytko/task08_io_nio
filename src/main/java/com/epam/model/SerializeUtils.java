package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SerializeUtils {
    private static final String FILE_NAME = "";
    private static final Logger LOGGER = LogManager.getLogger(ReaderUtils.class);
    private User user = new User("Bob", "Biliam", "Marley", 32, "USA");


    public void writeObject() {
        user.staticValue = 10;
        try (ObjectOutputStream objOut = new ObjectOutputStream(
                Files.newOutputStream(Paths.get(FILE_NAME)))
        ) {
        } catch (IOException e) {
            LOGGER.error("File cannot be opened. Program terminates");
            e.printStackTrace();
        }
    }

    public void readObject() {
        try (ObjectInputStream objInput = new ObjectInputStream(
                Files.newInputStream(Paths.get(FILE_NAME))
        )) {
            User user = (User) objInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
