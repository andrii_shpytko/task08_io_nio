package com.epam.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class ReverseInputStream extends InputStream {
    private byte[] image = new byte[0];
    private int remaining = 0;

    public ReverseInputStream(final InputStream inputStream) throws IOException {
        int available = inputStream.available();
        while (available > 0) {
            image = Arrays.copyOf(image, remaining + available);
            remaining += inputStream.read(image, remaining, available);
            available = inputStream.available();
        }

    }

    @Override
    public int read() {
        if (remaining == 0) {
            return -1;
        }
        remaining--;
        return image[remaining];
    }

    @Override
    public int read(final byte[] buffer, final int start, int count) {
        if (remaining == 0) {
            return -1;
        }
        if (remaining < count) {
            count = remaining;
        }
        for (int i = start; i < start + count; i++) {
            buffer[i] = (byte) read();
        }
        return count;
    }
}
