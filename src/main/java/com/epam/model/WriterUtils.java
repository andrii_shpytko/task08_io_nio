package com.epam.model;

import org.apache.commons.lang.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WriterUtils {
    private static final String FILE_NAME = "";
    private static final Logger LOGGER = LogManager.getLogger(WriterUtils.class);

    public void writeObject(User user) {
        try (ObjectOutputStream objOut = new ObjectOutputStream(
                Files.newOutputStream(Paths.get(FILE_NAME)))
        ) {
            objOut.writeObject(user);
            objOut.writeObject(new User("", "", "", 0, null));
        } catch (IOException e) {
            LOGGER.error("File cannot be opened. Program terminates");
            e.printStackTrace();
        }
    }

    public void usualWriting() {
        try (FileInputStream inputStream = new FileInputStream(
                new File(this.getClass().getResource("/40mb.txt").toURI()));
             FileOutputStream outputStream = new FileOutputStream(
                     new File(this.getClass().getResource("/40mb.txt").toURI()))
        ) {
            int c = 0;
            while ((c = inputStream.read()) != -1) {
                outputStream.write(c);
            }
        } catch (FileNotFoundException | URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedWriting() {
        try (BufferedInputStream inputStream = new BufferedInputStream(
                new FileInputStream(
                        new File(this.getClass().getResource("/100mb.txt").toURI())));
             BufferedOutputStream outputStream = new BufferedOutputStream(
                     new FileOutputStream(
                             new File(this.getClass().getResource("/text.txt").toURI())))
        ) {
            int c = 0;
            while ((c = inputStream.read()) != -1) {
                outputStream.write(c);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedWritingToSize(int size) {
        try (BufferedInputStream inputStream = new BufferedInputStream(
                new FileInputStream(
                        new File(this.getClass().getResource("/200mb.txt").toURI())));
             BufferedOutputStream outputStream = new BufferedOutputStream(
                     new FileOutputStream(
                             new File(this.getClass().getResource("/text.txt").toURI())
                     ), size)
        ) {
            int c = 0;
            while((c= inputStream.read()) != -1){
                outputStream.write(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        final long startTime = System.nanoTime();
        StopWatch timer = new StopWatch();
        timer.start();
        new WriterUtils().bufferedWritingToSize(150);
        timer.stop();
        long time = timer.getTime();
        final long duration = System.nanoTime() - startTime;
        LOGGER.info(time);
        LOGGER.info(duration / 1_000_000_000);

    }
}
